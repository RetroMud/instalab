# frozen_string_literal: true

require 'vagrant'

module InstaLab
  # Extend the Vagrant Plugin class and map the Config class
  class Plugin < Vagrant.plugin('2')
    name 'InstaLab'

    config 'lab' do
      Config
    end
  end
end
