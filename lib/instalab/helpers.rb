# frozen_string_literal: true

require 'pathname'

module InstaLab
  # Provide helper functions that need to operate both inside and outside of
  # the Vagrant environment
  module Helpers
    CONFIG_DIR = '.instalab'

    IPInformation = Struct.new(:family, :address, :netmask)

    def self.find_config
      user_home = File.expand_path('~')
      Pathname.new(File.join(user_home, CONFIG_DIR, 'default.json'))
    end

    def self.parse_cidr(ip_object)
      raise 'Expected an IPAddr object' unless ip_object.instance_of? IPAddr

      ip = ip_object.inspect
      data = ip.match(%r{<IPAddr:(?<family>[^:]*):(?<address>.*)\/(?<mask>.*)>})
      IPInformation.new(data[:family].strip, data[:address], data[:mask])
    end
  end
end
