# frozen_string_literal: true

require 'ipaddr'

module InstaLab
  # Represents a well defined local or remote network
  class Network
    class NetworkAddressError < StandardError
    end

    attr_reader :network_address, :cidr
    attr_reader :bridge_device
    attr_reader :address, :netmask

    def initialize(cidr, bridge_device = nil)
      @network_address = IPAddr.new(cidr)
      @cidr = cidr
      @bridge_device = bridge_device
      ip_info = InstaLab::Helpers.parse_cidr(@network_address)
      @address = ip_info.address
      @netmask = ip_info.netmask
    rescue IPAddr::InvalidPrefixError
      raise NetworkAddressError, 'Invalid network prefix'
    rescue IPAddr::InvalidAddressError
      raise NetworkAddressError, 'Invalid network address'
    end
  end
end
