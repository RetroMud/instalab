# frozen_string_literal: true

module InstaLab
  # Represents an individual machine on the network
  class Machine
    attr_reader :name
    attr_reader :host_id

    def initialize(name, host_id)
      @name = name
      @host_id = host_id
    end
  end
end
