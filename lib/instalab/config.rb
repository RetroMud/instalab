# frozen_string_literal: true

require 'vagrant'
require 'instalab/helpers'

module InstaLab
  # extends the Vagrant config plugin and provides data accessible via
  # config.lab.*
  class Config < Vagrant.plugin(2, :config)
    attr_accessor :config

    def initialize
      @config = InstaLab::Helpers.find_config
    end
  end
end
