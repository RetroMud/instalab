# frozen_string_literal: true

require 'instalab/plugin'
require 'instalab/config'
require 'instalab/machine'
require 'instalab/network'

# Top Level instance of the InstaLab module that stitches together all of
# the various components.
module InstaLab
end
