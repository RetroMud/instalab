# frozen_string_literal: true

RSpec.describe InstaLab do
  it 'has a version number' do
    expect(described_class::VERSION).not_to be nil
  end

  it 'has a Vagrant v2 plugin definition' do
    expect(described_class::Plugin).to be < Vagrant.plugin('2')
  end

  context 'using only default configuration' do
    let(:user_home) { '/home/kitten' }
    let(:config) { Pathname.new("#{user_home}/.instalab/default.json") }

    before do
      allow(File).to receive(:expand_path).with('~').and_return(user_home)
    end

    it 'tries to use the default configuration path' do
      lab = described_class::Config.new
      expect(lab.config).to eq(config)
    end
  end

  context 'using a custom configuration' do
    let(:config) { Pathname.new('/some/kinda/alternate/default.json') }

    before do
      @lab = described_class::Config.new
    end

    it 'stores the configuration filepath' do
      @lab.config = config
      expect(@lab.config).to eq(config)
    end
  end
end
