# frozen_string_literal: true

RSpec.describe InstaLab::Helpers do
  context '#parse_cidr' do
    let(:ipv4) { 'IPv4' }
    let(:cidr_24) { '10.0.0.0/24' }
    let(:netmask_24) { '255.255.255.0' }
    let(:cidr_16) { '10.0.0.0/16' }
    let(:netmask_16) { '255.255.0.0' }
    let(:ipv6) { 'IPv6' }
    let(:cidr_ipv6) { '2001:db8:abcd:234::/64' }
    let(:netmask_ipv6) { 'ffff:ffff:ffff:ffff:0000:0000:0000:0000' }

    before do
      @data24 = described_class.parse_cidr(IPAddr.new(cidr_24))
      @data16 = described_class.parse_cidr(IPAddr.new(cidr_16))
      @data_ipv6 = described_class.parse_cidr(IPAddr.new(cidr_ipv6))
    end

    it 'raises an error if passed something other than an IPAddr object' do
      expect { described_class.parse_cidr('Kittens') }.to raise_error(
        'Expected an IPAddr object'
      )
    end
    it 'returns the proper netmask against 24 bits' do
      expect(@data24.netmask).to eq(netmask_24)
      expect(@data24.family).to eq(ipv4)
    end
    it 'returns the proper netmask against 16 bits' do
      expect(@data16.netmask).to eq(netmask_16)
      expect(@data16.family).to eq(ipv4)
    end
    it 'returns the proper netmask against ipv6' do
      expect(@data_ipv6.netmask).to eq(netmask_ipv6)
      expect(@data_ipv6.family).to eq(ipv6)
    end
  end
end
