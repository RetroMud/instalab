# frozen_string_literal: true

RSpec.describe InstaLab::Machine do
  context 'when a machine object is created' do
    let(:machine_name) { 'node_for_yak_farming' }
    let(:host_id) { 42 }

    it 'correctly sets the machine name and host_id' do
      machine = described_class.new(machine_name, host_id)
      expect(machine.name).to eq(machine_name)
      expect(machine.host_id).to eq(host_id)
    end
  end
end
