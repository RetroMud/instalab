# frozen_string_literal: true

RSpec.describe InstaLab::Network do
  context 'initializing a new network object' do
    let(:cidr) { '10.10.10.0/24' }
    let(:improper_cidr) { '10.10.10.0/33' }
    let(:invalid_cidr) { 'kittenparty' }
    let(:device_bridge) { 'Some Wireless NIC' }
    let(:netmask) { '255.255.255.0' }
    it 'sets the cidr without a specified device bridge' do
      network = described_class.new(cidr)
      expect(network.cidr).to eq(cidr)
      expect(network.bridge_device).to eq(nil)
    end

    it 'sets a device bridge when specified' do
      network = described_class.new(cidr, device_bridge)
      expect(network.cidr).to eq(cidr)
      expect(network.bridge_device).to eq(device_bridge)
    end

    it 'handles improper network addresses' do
      expect { described_class.new(improper_cidr) }.to raise_error(
        described_class::NetworkAddressError, 'Invalid network prefix'
      )
    end

    it 'handles invalid network addresses' do
      expect { described_class.new(invalid_cidr) }.to raise_error(
        described_class::NetworkAddressError, 'Invalid network address'
      )
    end
  end
end
